// This is an example how to create a dripping effect using the FastLED library
#include "FastLED.h"

#define DATA_PIN    8
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

#define NUM_LEDS    289
#define SIDE_LEDS   82
#define UPPER_LEDS (NUM_LEDS-2*(SIDE_LEDS))
CRGB leds[NUM_LEDS];

#define A_POT_BRIGHTNESS 6
//#define BRIGHTNESS         127
#define FRAMES_PER_SECOND  60
#define LEAD_COUNT         SIDE_LEDS
#define TRAIL_LENGTH       20
#define TRAIL_DIM_BY       60
#define MIN_DELAY          500
#define MAX_DELAY          3000
#define DRIP_HUE           233
#define DRIP_HUE_SKEW      0
#define DRIP_SAT           127
#define MIRROR             true
#define MIRROR_INV_COLOR   true
#define STOP_SEGMENT       (SIDE_LEDS - 30)

// musi byt az tady, za definici NUM_LEDS a leds[]:
#include "LEDPasky_lib.h"

byte led_pos = 0;
byte inv_led_pos = NUM_LEDS-1;

void setup() {
//  delay(3000); // 3 second delay for recovery
  
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); // .setCorrection(Tungsten100W);
  
  // set master brightness control
  FastLED.setBrightness(0); // ridi se o kus dal potikem
  //FastLED.setBrightness(BRIGHTNESS);
  //randomSeed(analogRead(0));
}

byte trail_pos = 0;
byte lead_pos = 0;
CHSV color;
CHSV alt_color;

void loop() {
  set_dynamic_brightness_using_pot(analogRead(A_POT_BRIGHTNESS)); 
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  

  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND);
  
  fadeToBlackBy(leds, NUM_LEDS, TRAIL_DIM_BY);
  
  if (trail_pos == 0) {
    lead_pos++;
    if (lead_pos >= LEAD_COUNT) {
      color = CHSV(DRIP_HUE+(DRIP_HUE_SKEW*led_pos),DRIP_SAT,255);
      alt_color = CHSV(DRIP_HUE+(DRIP_HUE_SKEW*led_pos)+127,DRIP_SAT,255);
        leds[led_pos] = color;
        if (MIRROR) {
          if (MIRROR_INV_COLOR) leds[inv_led_pos] = alt_color;
            else leds[inv_led_pos] = color;
        }
      inv_led_pos--;
      led_pos++;
    } else {
      CHSV color_1 = CHSV(DRIP_HUE+LEAD_COUNT-lead_pos,DRIP_SAT,map(lead_pos, 1, LEAD_COUNT, 1, 255));
      CHSV color_2 = CHSV(DRIP_HUE+LEAD_COUNT-lead_pos,DRIP_SAT,map(lead_pos, 1, LEAD_COUNT, 1, 64));
      CHSV alt_color_1 = CHSV(DRIP_HUE+LEAD_COUNT-lead_pos+127,DRIP_SAT,map(lead_pos, 1, LEAD_COUNT, 1, 255));
      CHSV alt_color_2 = CHSV(DRIP_HUE+LEAD_COUNT-lead_pos+127,DRIP_SAT,map(lead_pos, 1, LEAD_COUNT, 1, 64));
      
      if (MIRROR) {
        leds[inv_led_pos-1] = color_2;
        leds[inv_led_pos] = color_1;
        if (MIRROR_INV_COLOR) {
          leds[inv_led_pos-1] = alt_color_2;
          leds[inv_led_pos] = alt_color_1;
        }
      }
      leds[led_pos] = color_1;
      leds[led_pos+1] = color_2;
    }
  }
  
  if (led_pos >= STOP_SEGMENT-1) {
    trail_pos++;
    if (trail_pos <= TRAIL_LENGTH*0.50) {
      leds[led_pos] = color;
      if (MIRROR) {
        leds[inv_led_pos] = color;
        if (MIRROR_INV_COLOR) {
          leds[inv_led_pos] = alt_color;
        }
      }
    }
    if (trail_pos >= TRAIL_LENGTH) {
      leds[led_pos] = CHSV(0,0,0);
      if (MIRROR) leds[inv_led_pos] = CHSV(0,0,0);
        FastLED.delay(random(MIN_DELAY, MAX_DELAY));
        trail_pos = 0;
        led_pos = 0;
        lead_pos = 0;
        inv_led_pos = NUM_LEDS-1;
      }
  }
}
