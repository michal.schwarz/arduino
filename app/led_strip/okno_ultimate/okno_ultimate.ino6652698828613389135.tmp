#include "FastLED.h"

FASTLED_USING_NAMESPACE

// FastLED "100-lines-of-code" demo reel, showing just a few 
// of the kinds of animation patterns you can quickly and easily 
// compose using FastLED.  
//
// This example also shows one easy way to define multiple 
// animations patterns and have them automatically rotate.
//
// -Mark Kriegsman, December 2014

#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    8
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    289
#define SIDE_LEDS   82
#define UPPER_LEDS (NUM_LEDS-2*(SIDE_LEDS))
CRGB leds[NUM_LEDS];

#define FRAMES_PER_SECOND  120

// musi byt az tady, za definici NUM_LEDS:
#include "LEDPasky_lib.h"

void setup() {
  // Serial.begin(115200);
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); // .setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(0);
}


// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { sinelon, juggle, ocean, bpm, confetti, rainbowWithGlitter,  };  //, rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm };

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void loop()
{
  // Call the current pattern function once, updating the 'leds' array
  gPatterns[gCurrentPatternNumber]();

  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND); 

  // do some periodic updates
  EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow

  // jednim potikem menime efekty:
  int p = analogRead(7);  // 0 - 1023
  if (p < 512) {
    EVERY_N_SECONDS( 15 ) { nextPattern(); } // change patterns periodically
  } else {  // 512-1023
    // gCurrentPatternNumber = (p * 6 / 512);  // 0 - 5, resp. pocet 
    gCurrentPatternNumber = map(p, 512, 1023, 0, ARRAY_SIZE( gPatterns)-1);
  }

  // druhym menime jas:
  set_dynamic_brightness_using_pot(analogRead(6)); 
}

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}

// To, co je prave zobrazene v [0 .. SIDE_LEDS-1] zkopiruje i na protejsi stranu
void mirror_side() {
  for (int i=0; i<SIDE_LEDS; i++) {
    leds[NUM_LEDS-i-1] = leds[i];
  }
};

// Kompletne celou pravou stranu prouzku zkopiruje na druhou stranu:
void mirror_all() {
  for (int i=0; i<(NUM_LEDS/2); i++) {
    leds[NUM_LEDS-i-1] = leds[i];
  }
};

void rainbow() 
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void ocean() 
{
  FillLEDsFromPaletteColors(OceanColors_p, gHue);
}

void rainbowWithGlitter() 
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti() 
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

// nevyplnuje pos1, pokud to neni nutne
void add_fill_from_prevpos(int pos1, int pos2, CRGB color, uint8_t mixmode) {
  if (pos2 >= pos1) {
    for (int i=pos1+1; i<=pos2; i++) {
      if (mixmode == 0) {
        leds[i] += color;
      } else if (mixmode == 1) {
        leds[i] |= color;
      };
    };
  } else {
    for (int i=pos1-1; i>=pos2; i--) {
      if (mixmode == 0) {
        leds[i] += color;
      } else if (mixmode == 1) {
        leds[i] |= color;
      };
    };
  };
};

void sinelon()  // jedna tečka, která jen mění barvu a běhá po proužku
{
  // a colored dot sweeping back and forth, with fading trails
  static int prevpos = 0;
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16(13,0,NUM_LEDS); // 13x za minutu proběhne skrz celý proužek (ale ... samozřejmě občas některé LEDky přeskočí, protože beatsin16 logicky nemusí vracet těsně navazující hodnoty

   // mixmode: +=
  add_fill_from_prevpos(prevpos, pos, CHSV(gHue, 255, 192), 0); // vzdy vyplni cely prostor od predchoziho bodu k novemu
  // gHue++; // jen test, pak bude menit barvu i ta samotna cara
  prevpos = pos;
  // mirror_all();
}

// FastLED provides several 'preset' palettes: RainbowColors_p, RainbowStripeColors_p,
// OceanColors_p, CloudColors_p, LavaColors_p, ForestColors_p, and PartyColors_p.
void bpm() // "písty", jak by to popsal Fífa
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10)); // paleta, colorindex, brightness, blending (LINEARBLEND)
  }
  //mirror_all();
}

void juggle() {   // tečky se vzájemně honí po celém proužku
  // eight colored dots, weaving in and out of sync with each other
  static int prevpos[8];

  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    // leds[beatsin16(i+7,0,NUM_LEDS)] |= CHSV(dothue, 200, 255);
    int pos = beatsin16(i+7,0,NUM_LEDS);
    add_fill_from_prevpos(prevpos[i], pos, CHSV(dothue, 200, 255), 1);  // mixmode: |=
    prevpos[i] = pos;
    dothue += 32;
  }
//  mirror_all();  
}

void FillLEDsFromPaletteColors(CRGBPalette16 palette, uint8_t colorIndex)
{
    uint8_t brightness = 255;
    
    for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette(palette, colorIndex, brightness, LINEARBLEND);
        colorIndex += 1;
    }
}

