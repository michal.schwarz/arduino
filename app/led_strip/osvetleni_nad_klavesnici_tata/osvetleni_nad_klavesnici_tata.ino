#include "FastLED.h"
FASTLED_USING_NAMESPACE

// FastLED "100-lines-of-code" demo reel, showing just a few 
// of the kinds of animation patterns you can quickly and easily 
// compose using FastLED.  
//
// This example also shows one easy way to define multiple 
// animations patterns and have them automatically rotate.
//
// -Mark Kriegsman, December 2014

#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    2
//#define CLK_PIN   4
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    7
CRGB leds[NUM_LEDS];

// musi byt az tady, za definici NUM_LEDS:
#include "LEDPasky_lib.h"

#define FRAMES_PER_SECOND  1000

void setup() {
  //Serial.begin(115200);
  // delay(3000); // 3 second delay for recovery

  pinMode(3, INPUT_PULLUP);
  pinMode(4, OUTPUT);  // prostredni pin tlacitka
  pinMode(5, INPUT_PULLUP);
  digitalWrite(4, LOW);
//  Serial.println("ready:");

  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); // .setCorrection(TypicalLEDStrip);

  for (int i=0; i<NUM_LEDS; i++) {
    leds[i] = CRGB::White;
  }

  // set master brightness control
  FastLED.setBrightness(255);

  // teplejsi bila:
  // http://fastled.io/docs/3.1/group___color_enums.html
  FastLED.setTemperature(Tungsten40W);
  FastLED.setTemperature(Candle);
}

// variables will change:
int buttonState1 = 0;         // variable for reading the pushbutton status
int buttonState2 = 0;         // variable for reading the pushbutton status
int laststate = 0;
int newstate = 0;

int b = 255;  // brightness;

void loop()
{
  buttonState1 = digitalRead(3);
  buttonState2 = digitalRead(5);
  newstate = (buttonState1 << 1) | buttonState2;

  if (newstate != laststate) {
    if (newstate != 3) {  // stisknuto
      if (b == 0) {
        // 0 -> 255
        b = 255;
      } else if (b >= 50) {
        // zmenseni jasu o 32
        b -= 50;
      } else {
        // doladeni na 0 (pro hodnoty 1-31)
        b = 0;
      } 
      laststate = newstate;
    };
  }
  
//  Serial.print("1=");
//  Serial.print(buttonState1);
//  Serial.print(", 2=");
//  Serial.print(buttonState2);
//  Serial.println("");
  //Serial.print("state=");
  //Serial.print(newstate);
  //Serial.println("");
  
//  leds[6] = leds[4] = leds[2] = leds[0];
//  leds[7] = leds[5] = leds[3] = leds[1];

  FastLED.setBrightness(b);
    
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  FastLED.delay(1000/FRAMES_PER_SECOND); 
}
