#include "FastLED.h"

FASTLED_USING_NAMESPACE

#define DATA_PIN    8
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

#define NUM_LEDS    289
#define SIDE_LEDS   83
#define UPPER_LEDS (NUM_LEDS-2*(SIDE_LEDS))
CRGB leds[NUM_LEDS];

#define A_POT_BRIGHTNESS 6
#define FRAMES_PER_SECOND  60

#define DELKA_RAMPOUCHU   30

// musi byt az tady, za definici NUM_LEDS a leds[]:
#include "LEDPasky_lib.h"

byte led_pos = 0;
byte inv_led_pos = NUM_LEDS-1;

void setup() {
//  delay(3000); // 3 second delay for recovery
  
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); // .setCorrection(Tungsten100W);
  
  // set master brightness control
  FastLED.setBrightness(0); // ridi se o kus dal potikem
}

char kapka_faze = 'k';  // 'k' == klouze, 'r' = roste, 'p' = pada
int kapka_pos = 0;
int kapka_rust = 0;
int kapka_jas = 160;
int kapka_pauza = 0;

int posun_horni = 0;
void loop() {
  fill_solid(leds, NUM_LEDS, CRGB::Black);  // at vzdy zaciname s cistym stitem

  // horni pruh modry:
  //fill_solid(&leds[SIDE_LEDS], UPPER_LEDS, CRGB::Aqua);
//  fill_solid(&leds[SIDE_LEDS], UPPER_LEDS, CRGB::DarkRed);

  for (int i=0; i<UPPER_LEDS; i++) {
     leds[i+SIDE_LEDS] = CRGB::DarkGreen;
     if (((i+posun_horni)/6) % 2) {
       leds[i+SIDE_LEDS] = CRGB::DarkRed;
     }
  };
  //posun_horni++;
  
  // priprava rampouchu:
  for (int i=0; i<DELKA_RAMPOUCHU; i++) {
    leds[SIDE_LEDS-i-1] = CHSV(HUE_BLUE, 255, 255 - (255/DELKA_RAMPOUCHU)*i);
  }

  // posouvani kapky:
  if (kapka_faze == 'k') { // klouze
    leds[SIDE_LEDS-kapka_pos-1] = CHSV(0,0,kapka_jas);
    kapka_pos++;
    if (kapka_pos >= DELKA_RAMPOUCHU) {
      kapka_faze = 'r';
      kapka_rust = 0;
    }
  } else if (kapka_faze == 'r') { // roste
    leds[SIDE_LEDS-kapka_pos-1] = CHSV(0,0,kapka_jas);
    kapka_rust++;
    if (kapka_rust < 60) {
      kapka_jas = 127 + 2*kapka_rust;
    } else {
      kapka_faze = 'p';
    };
  } else if (kapka_faze == 'p') { // pada
    leds[SIDE_LEDS-kapka_pos-1] = CHSV(0,0,kapka_jas);
    kapka_pos++;
    kapka_pos++;
    kapka_pos++;
    if (kapka_pos >= SIDE_LEDS-2) {
      kapka_pos = 0;
      kapka_jas = 160;
      kapka_pauza = random16(20,200);
      kapka_faze = 'c';
    }
  } else if (kapka_faze == 'c') { // ceka
     if (kapka_pauza-- == 0) {
      kapka_faze = 'k';
     }
  };

  
  // kopie cele prave strany na levou:
  for (int i=0; i<SIDE_LEDS; i++) {
    leds[NUM_LEDS-1-i] = leds[i];
  }
  
  //fadeToBlackBy(leds, NUM_LEDS, TRAIL_DIM_BY);


  
  set_dynamic_brightness_using_pot(analogRead(A_POT_BRIGHTNESS)); 
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND);
}
