#include "FastLED.h"
FASTLED_USING_NAMESPACE

// FastLED "100-lines-of-code" demo reel, showing just a few 
// of the kinds of animation patterns you can quickly and easily 
// compose using FastLED.  
//
// This example also shows one easy way to define multiple 
// animations patterns and have them automatically rotate.
//
// -Mark Kriegsman, December 2014

#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    8
//#define CLK_PIN   4
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    8
CRGB leds[NUM_LEDS];

// musi byt az tady, za definici NUM_LEDS:
#include "LEDPasky_lib.h"

#define FRAMES_PER_SECOND  1

void setup() {
  Serial.begin(115200);
  // delay(3000); // 3 second delay for recovery
  
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); // .setCorrection(TypicalLEDStrip);

  // set master brightness control
  FastLED.setBrightness(255);
}

int faze = 0;
void loop()
{
  if (faze == 0) {
    leds[0] = CRGB::Red;
    leds[1] = CRGB::Green;
    faze = 1;
  } else {
    leds[0] = CRGB::Green;
    leds[1] = CRGB::Red;
    faze = 0;
  };
  leds[6] = leds[4] = leds[2] = leds[0];
  leds[7] = leds[5] = leds[3] = leds[1];
  
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  FastLED.delay(1000/FRAMES_PER_SECOND); 
}



