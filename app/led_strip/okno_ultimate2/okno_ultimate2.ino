#include "FastLED.h"

FASTLED_USING_NAMESPACE

// FastLED "100-lines-of-code" demo reel, showing just a few 
// of the kinds of animation patterns you can quickly and easily 
// compose using FastLED.  
//
// This example also shows one easy way to define multiple 
// animations patterns and have them automatically rotate.
//
// -Mark Kriegsman, December 2014

#if FASTLED_VERSION < 3001000
#error "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define NUM_LEDS    289
#define SIDE_LEDS   82
#define UPPER_LEDS (NUM_LEDS-2*(SIDE_LEDS))

#define DATA_PIN    8
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

#define A_POT_BRIGHTNESS 6
#define A_POT_EFFECTS 7

// digitalni vstupy:
#define D_BUTTON_PIN_A  12  // zluta
#define D_BUTTON_PIN_B  9   // cervena
#define D_BUTTON_PIN_C  10  // zelena
#define D_BUTTON_PIN_D  11  // modra

#define FRAMES_PER_SECOND  120
#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

// musi byt az tady, za definici NUM_LEDS a leds[]:
#include "LEDPasky_lib.h"

CRGBPalette16 paleta_ohne[] = {
  CRGBPalette16( CRGB::Black, CRGB::Red, CRGB::Yellow, CRGB::White),
  // CRGBPalette16( CRGB::Black, CRGB::DarkGreen, CRGB::Chartreuse, CRGB::PaleGreen),  
  CRGBPalette16( CRGB::Black, CRGB::Chartreuse, CRGB::Green, CRGB::ForestGreen),
  // CRGBPalette16( CRGB::Black, CRGB::Blue, CRGB::LightSeaGreen,  CRGB::PaleTurquoise),
  CRGBPalette16( CRGB::Black, CRGB::DarkMagenta, CRGB::MediumPurple,  CRGB::White),
  // CRGBPalette16( CRGB::Black, CRGB(71,180,213), CRGB(155,249,249), CRGB(255,255,147)) // podle http://www.colourlovers.com/palette/3001360/Blue_Fire_Blaze
};

CRGBPalette16 paleta_jen_tak[] = { OceanColors_p, CloudColors_p, LavaColors_p };  // , ForestColors_p, PartyColors_p, HeatColors_p};  // , RainbowColors_p, RainbowStripeColors_p, };

struct Tmin_max_default {    int min;    int dfl;    int max;};
// int value2pot(int v, Tmin_max_default ranges) {   return map(constrain(v, ranges.min, ranges.max), ranges.min, ranges.max, 0, 1023); }
int pot2value(int pot, Tmin_max_default ranges) {     
  //return map(pot, 0, 1023, ranges.min, ranges.max);  
  return map(constrain(pot, 0, 1000), 0, 1000, ranges.min, ranges.max);   // prozatim aspon takhle, nez vyresim to spravne mapovani rozsahu
}

#define REAGUJ_NA_TLACITKO(btn_pin, pot_pin, varname, xmin, xdefault, xmax) \
static Tmin_max_default t_##varname = {xmin, xdefault, xmax}; \
static int varname = (t_##varname).dfl; \
{ \
  if (tlacitko_stisknuto(btn_pin)) { \
    varname = pot2value(analogRead(pot_pin), t_##varname); \
  } \
};


void setup() {
  Serial.begin(115200);

  pinMode(D_BUTTON_PIN_A, INPUT_PULLUP);
  pinMode(D_BUTTON_PIN_B, INPUT_PULLUP);
  pinMode(D_BUTTON_PIN_C, INPUT_PULLUP);
  pinMode(D_BUTTON_PIN_D, INPUT_PULLUP);
  
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); // .setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(0);
}

// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { sinelon, juggle, paleta, bpm, confetti, rainbowWithGlitter, fire, };  //, rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm };

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

void loop()
{
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_D, A_POT_EFFECTS, efekt, -1, -1, ARRAY_SIZE( gPatterns)-1); // modra meni efekt
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_C, A_POT_EFFECTS, ghue_delta, -20, 1, 20);  // zelena meni hlavni odstin

  // do some periodic updates
  EVERY_N_MILLISECONDS( 20 ) { gHue += ghue_delta; } // slowly cycle the "base color" through the rainbow
  if (efekt < 0) {
    EVERY_N_SECONDS( 15 ) { nextPattern(); } // change patterns periodically
  } else {  // 512-1023
    gCurrentPatternNumber = efekt;
  }

  // Call the current pattern function once, updating the 'leds' array
  gPatterns[gCurrentPatternNumber]();

  // send the 'leds' array out to the actual LED strip
  set_dynamic_brightness_using_pot(analogRead(A_POT_BRIGHTNESS)); 
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND); 
}

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}

void rainbow() 
{
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_A, A_POT_EFFECTS, hustota_duhy, 1, 7, 60);
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, hustota_duhy);
}

void rainbowWithGlitter() 
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti() 
{
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_A, A_POT_EFFECTS, fade_speed, 255, 10, 5);
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, fade_speed);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()  // jedna tečka, která jen mění barvu a běhá po proužku
{
  // a colored dot sweeping back and forth, with fading trails
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_A, A_POT_EFFECTS, bpm, 1, 13, 30);
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_B, A_POT_EFFECTS, fade_speed, 255, 20, 10);

  static int prevpos = 0;
  fadeToBlackBy( leds, NUM_LEDS, fade_speed);
  int pos = beatsin16(bpm,0,NUM_LEDS); // 13x za minutu proběhne skrz celý proužek (ale ... samozřejmě občas některé LEDky přeskočí, protože beatsin16 logicky nemusí vracet těsně navazující hodnoty

  // mixmode: +=
  add_fill_from_prevpos(prevpos, pos, CHSV(gHue, 255, 192), 0); // vzdy vyplni cely prostor od predchoziho bodu k novemu
  prevpos = pos;
  // mirror_all();
}

// FastLED provides several 'preset' palettes: RainbowColors_p, RainbowStripeColors_p,
// OceanColors_p, CloudColors_p, LavaColors_p, ForestColors_p, and PartyColors_p.
void bpm() // "písty", jak by to popsal Fífa
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_A, A_POT_EFFECTS, bpm, 10, 62, 120);

  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8(bpm, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10)); // paleta, colorindex, brightness, blending (LINEARBLEND)
  }
  //mirror_all();
}

void juggle() {   // 8 teček se vzájemně honí po celém proužku
  // eight colored dots, weaving in and out of sync with each other
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_A, A_POT_EFFECTS, fade_speed, 200, 20, 5);
  fadeToBlackBy( leds, NUM_LEDS, fade_speed);
  byte dothue = 0;
  static int prevpos[8];
  for( int i = 0; i < 8; i++) {
    // leds[beatsin16(i+7,0,NUM_LEDS)] |= CHSV(dothue, 200, 255);
    int pos = beatsin16(i+7,0,NUM_LEDS);
    add_fill_from_prevpos(prevpos[i], pos, CHSV(dothue, 200, 255), 1);  // mixmode: |=
    prevpos[i] = pos;
    dothue += 32;
  }
//  mirror_all();  
}


void paleta() {
    REAGUJ_NA_TLACITKO(D_BUTTON_PIN_A, A_POT_EFFECTS, cislo_palety, 0, 0, ARRAY_SIZE(paleta_jen_tak)-1);    
    REAGUJ_NA_TLACITKO(D_BUTTON_PIN_B, A_POT_EFFECTS, hustota_palety, 1, 1, 8);    
    FillLEDsFromPaletteColors(paleta_jen_tak[cislo_palety], gHue, hustota_palety, LINEARBLEND);
}

void fire() {
  random16_add_entropy( random());
  Fire2012WithPalette(); // run simulation frame, using palette colors
  //mirror_side();
}

// A tohle je skutecna cast ktera pocita ten ohen:
// #define COOLING  85
#define COOLING cooling
#define SPARKING 120
void Fire2012WithPalette()
{
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_A, A_POT_EFFECTS, ktera_paleta_ohne, 0, 0, ARRAY_SIZE(paleta_ohne)-1);    
  REAGUJ_NA_TLACITKO(D_BUTTON_PIN_B, A_POT_EFFECTS, cooling, 200, 85, 20);

// Array of temperature readings at each simulation cell
  static byte heat1[SIDE_LEDS];
  static byte heat2[SIDE_LEDS];

  // Step 1.  Cool down every cell a little
    for( int i = 0; i < SIDE_LEDS; i++) {
      heat1[i] = qsub8( heat1[i],  random8(0, ((COOLING * 10) / SIDE_LEDS) + 2));
      heat2[i] = qsub8( heat2[i],  random8(0, ((COOLING * 10) / SIDE_LEDS) + 2));
    }
  
    // Step 2.  Heat from each cell drifts 'up' and diffuses a little
    for( int k= SIDE_LEDS - 1; k >= 2; k--) {
      heat1[k] = (heat1[k - 1] + heat1[k - 2] + heat1[k - 2] ) / 3;
      heat2[k] = (heat2[k - 1] + heat2[k - 2] + heat2[k - 2] ) / 3;
    }
    
    // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
    if( random8() < SPARKING ) {
      int y = random8(7);
      heat1[y] = qadd8( heat1[y], random8(160,255) );
      y = random8(7);
      heat2[y] = qadd8( heat2[y], random8(160,255) );
    }

    // Step 4.  Map from heat cells to LED colors
    // pozor, tady sice ohen generuji jen pro SIDE_LEDS, ale mazu samozrejme i zbytek pasku (pro cele NUM_LEDS):
    fill_solid(leds, NUM_LEDS, CRGB::Black);
    for( int j = 0; j < SIDE_LEDS; j++) {
        // Scale the heat value from 0-255 down to 0-240
        // for best results with color palettes.
        byte colorindex1 = scale8( heat1[j], 240);
        byte colorindex2 = scale8( heat2[j], 240);
        CRGB color1 = ColorFromPalette( paleta_ohne[ktera_paleta_ohne], colorindex1);
        CRGB color2 = ColorFromPalette( paleta_ohne[ktera_paleta_ohne], colorindex2);
        
        int pixelnumber;
        pixelnumber = j;
        leds[pixelnumber] = color1;
        
        pixelnumber = NUM_LEDS - j - 1;
        leds[pixelnumber] = color2;
    }
}

void FillLEDsFromPaletteColors(CRGBPalette16 palette, uint8_t colorIndex, uint8_t step, TBlendType blending)
{
    uint8_t brightness = 255;
    for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette(palette, colorIndex, brightness, blending);
        colorIndex += step;
    }
}

// nevyplnuje pos1, pokud to neni nutne
void add_fill_from_prevpos(int pos1, int pos2, CRGB color, uint8_t mixmode) {
  if (pos1 == pos2) {
    if (mixmode == 0) {
      leds[pos2] += color;
    } else if (mixmode == 1) {
      leds[pos2] |= color;
    };
  } else if (pos2 > pos1) {
    for (int i=pos1+1; i<=pos2; i++) {
      if (mixmode == 0) {
        leds[i] += color;
      } else if (mixmode == 1) {
        leds[i] |= color;
      };
    };
  } else {
    for (int i=pos1-1; i>=pos2; i--) {
      if (mixmode == 0) {
        leds[i] += color;
      } else if (mixmode == 1) {
        leds[i] |= color;
      };
    };
  };
};

uint8_t tlacitko_stisknuto(int pin) { 
  uint8_t ret = 0;
  int state = digitalRead(pin);
  if (state == LOW) {
    ret = 1;
  };
  return ret;
}


// To, co je prave zobrazene v [0 .. SIDE_LEDS-1] zkopiruje i na protejsi stranu
void mirror_side() {
  for (int i=0; i<SIDE_LEDS; i++) {
    leds[NUM_LEDS-i-1] = leds[i];
  }
};

// Kompletne celou pravou stranu prouzku zkopiruje na druhou stranu:
void mirror_all() {
  for (int i=0; i<(NUM_LEDS/2); i++) {
    leds[NUM_LEDS-i-1] = leds[i];
  }
};


