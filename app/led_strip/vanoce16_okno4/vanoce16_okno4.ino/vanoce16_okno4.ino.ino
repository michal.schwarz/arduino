#include "FastLED.h"

FASTLED_USING_NAMESPACE

#define DATA_PIN    8
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

#define NUM_LEDS    289
#define SIDE_LEDS   83
#define UPPER_LEDS (NUM_LEDS-2*(SIDE_LEDS))
CRGB leds[NUM_LEDS];

#define A_POT_BRIGHTNESS 6
#define FRAMES_PER_SECOND  60

// musi byt az tady, za definici NUM_LEDS a leds[]:
#include "LEDPasky_lib.h"

byte led_pos = 0;
byte inv_led_pos = NUM_LEDS-1;

struct Trampouch {
  char kapka_faze = 'k';  // 'k' == klouze, 'r' = roste, 'p' = pada
  
  float kapka_pos = 0;
  float kapka_rychlost = 0.1;
  float kapka_zrychleni = 0.1;
  
  int kapka_rust = 0;
  int kapka_jas = 192;
  int kapka_pauza = 0;
};

void setup() {
//  delay(3000); // 3 second delay for recovery
  
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); // .setCorrection(Tungsten100W);
  
  // set master brightness control
  FastLED.setBrightness(0); // ridi se o kus dal potikem
};

int xpos(int start_idx, bool reverse, int virtpos) {  // vrati pozici, but regulerni nebo mirrorovanou
  if (reverse) {
    return start_idx - virtpos;
  } else {
    return start_idx + virtpos;
  }
};
#define VIRT_LED(i) leds[xpos(start_idx,reverse,i)]

// pozice_v_palete, R, G, B:
DEFINE_GRADIENT_PALETTE(pal_rampouch1) {
   0, 255,255,255,  // bila
   127, 0,0,255, // modra
   255, 0,0,0 // cerna
};
//CRGBPalette16 pal = pal_rampouch1;
CRGBPalette16 pal = CRGBPalette16(CRGB(192,192,192), CRGB(0,0,192), CRGB::Black);

void rampouch_single_frame(struct Trampouch *r, struct Trampouch *r_default, int delka_komplet, int delka_rampouchu, int start_idx, bool reverse) {
  // priprava rampouchu:
  for (int i=0; i<delka_rampouchu; i++) {
    // nemuzu az do 255, pak mi to zacne tahat barvu zase ze zacatku palety, kvuli blendingu, tzn. bilou
    VIRT_LED(i) = ColorFromPalette(pal, map(i, 0, delka_rampouchu, 0, 230), 255, LINEARBLEND); // paleta, pozice (0-255), jas, blending
  };
  
  // posouvani kapky:
  if (r->kapka_faze == 'k') { // klouze
    VIRT_LED(r->kapka_pos) = CHSV(0,0,r->kapka_jas);
    r->kapka_pos += r->kapka_rychlost;
    r->kapka_rychlost += r->kapka_zrychleni;
    if (r->kapka_pos >= delka_rampouchu) {
      r->kapka_pos = delka_rampouchu;
      r->kapka_faze = 'r';
    }
  } else if (r->kapka_faze == 'r') { // roste
    VIRT_LED(r->kapka_pos) = CHSV(0,0,r->kapka_jas);
    r->kapka_rust++;
    r->kapka_jas = r_default->kapka_jas + 4*r->kapka_rust;
    if (r->kapka_jas >= 255) {
      r->kapka_jas = 255;
      r->kapka_faze = 'p';
    };
  } else if (r->kapka_faze == 'p') { // pada
    VIRT_LED(r->kapka_pos) = CHSV(0,0,r->kapka_jas);
    r->kapka_pos += r->kapka_rychlost;
    r->kapka_rychlost += r->kapka_zrychleni;
    if (r->kapka_pos >= delka_komplet) {  // jakmile by kapka spadla az za konec retezu
      r->kapka_pauza = random16(20,200);
      r->kapka_faze = 'c';
    }
  } else if (r->kapka_faze == 'c') { // cekani, kapka se nezobrazuje
     if (r->kapka_pauza-- == 0) {
      // reset vychozich hodnot:
      r->kapka_pos = r_default->kapka_pos;
      r->kapka_rychlost = r_default->kapka_rychlost;
      r->kapka_zrychleni = r_default->kapka_zrychleni; 
      r->kapka_rust = r_default->kapka_rust;
      r->kapka_jas = r_default->kapka_jas;      
      // a prepnuti opet na kapani:
      r->kapka_faze = 'k';
     }
  };
};

Trampouch rampouch1; Trampouch rampouch1_default;
Trampouch rampouch2; Trampouch rampouch2_default;

int posun_horni = 0;
void loop() {
  fill_solid(leds, NUM_LEDS, CRGB::Black);  // at vzdy zaciname s cistym stitem

  for (int i=0; i<UPPER_LEDS; i++) {
     leds[i+SIDE_LEDS] = CRGB::DarkGreen;
     if (((i+posun_horni)/5) % 2) {
       leds[i+SIDE_LEDS] = CRGB::DarkRed;
     }
  };
  posun_horni++;

  rampouch_single_frame(&rampouch2, &rampouch2_default, SIDE_LEDS, 30, SIDE_LEDS+UPPER_LEDS, false); // levy
  rampouch_single_frame(&rampouch1, &rampouch1_default, SIDE_LEDS, 30, SIDE_LEDS-1, true); //pravy
  
  set_dynamic_brightness_using_pot(analogRead(A_POT_BRIGHTNESS)); 
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND);
}
