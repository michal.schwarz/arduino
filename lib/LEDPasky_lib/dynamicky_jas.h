/* specialni a komplikovany kvantovy vypocet casoprostorove deformace, potrebne ke zjisteni maximalniho jasu :-) */
#define POWER_SOURCE_mA 4000
#define SINGLE_PART_mA 20
int max_dynamic_brightness(int required_b) {
  int total_mA = 0;  // pokud jedna LEDka zere max 60 mA, tak by to prelezlo int az pri 32767/60 = 546 LEDkach. Takze pouziju int a ne long.
  for (int i=0; i<NUM_LEDS; i++) {
     int rgb = (int)leds[i].r + (int)leds[i].g + (int)leds[i].b; // 0 - (3*255) = 0 - 767
     //Serial.print("LED["); Serial.print(i); Serial.print("]=");
     //Serial.print(rgb);
     //Serial.print(",r=");Serial.print(leds[i].r);
     //Serial.print(",g=");Serial.print(leds[i].g);
     //Serial.print(",b=");Serial.print(leds[i].b);
     int mA = 1 + (rgb * SINGLE_PART_mA) / 256;  // 1 - 60
     total_mA += mA;
  };
  
  int max_b = 255;
  if (total_mA > POWER_SOURCE_mA) { // kdyz to i PO ZKORIGOVANI JASU zere moc, tak jak moc to zkorigovat?
    max_b = ((long)max_b * POWER_SOURCE_mA) / total_mA; // zde ale musim pracovat s LONG vypocty
  };

  int b = required_b;  // max. jas (0-255)
  if (b < 0) {
    b = 0;
  } else if (b > 255) {
    b = 255;
  }
  if (b > max_b) { // je-li ten pozadovany jas (nastaveny potikem) vetsi nez ktery zvladne zdroj, zarizneme to:
    b = max_b;
  };

/*
  Serial.print("LEDek=");
  Serial.print(NUM_LEDS);
  Serial.print(", mA=");
  Serial.print(total_mA);
  Serial.print(", limit_mA=");
  Serial.print(POWER_SOURCE_mA);
  Serial.print(", req_b=");
  Serial.print(required_b);
  Serial.print(", new_b=");
  Serial.print(b);
  Serial.println("");
*/

  return b;
};
void set_dynamic_brightness_using_pot(int pot_val) {
    FastLED.setBrightness(max_dynamic_brightness(map(pot_val, 0, 1023, 0, 255)));  // konverze z rozsahu 0-1023 do rozsahu 0-255
};

  

